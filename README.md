## Firmware Code Signing

Create private key and keep you pirvate key safe
```shell
openssl ecparam -name secp256k1 -genkey -noout -out private.pem
openssl ec -in private.pem -pubout -out public.pem
```

Sign firmware

```shell
openssl dgst -sha256 -sign private.pem -out firmware.sig firmware
```

Validate firmware
```shell
openssl dgst -sha256 -verify public.pem -signature firmware.sig firmware
  or on Wolf Controller
wolf_fw_validator firmware public.pem firmware.sig
```
In an src folder is an example of how to validate firmware with Mbed TLS
