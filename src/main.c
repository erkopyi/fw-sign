#include <mbedtls/sha256.h>
#include <string.h>
#include "mbedtls/ecdsa.h"
#include "mbedtls/pk.h"

#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 4096
#define HASH_SIZE 32

// reference: https://interrupt.memfault.com/blog/secure-firmware-updates-with-code-signing#bundling-signatures-with-our-builds

static char *program_name;
int exit_stat;

void help(void) {
	fprintf(stderr, "Firmware Signature Validator\n" \
		"Usage: %s <firmware image> <public key> <signature>\n" \
		, program_name
	);
}

int open_file (FILE **fp, char *file_name) {
	*fp = fopen(file_name, "r");
	if (*fp == NULL) {
		fprintf(stderr, "failed to open file :%s\n", file_name);
		help();
		return(1); 
	}
	return(0);
}

int get_file_size (FILE *f) {
	fseek(f, 0, SEEK_END);
	int size = ftell(f);
	fseek(f, 0, SEEK_SET);
	return(size);
}

int main(int argc, char *argv[]) {
	int ret, len;
	FILE *fimg, *fpub, *fsig;
	uint8_t hash[HASH_SIZE];
	char *spub, *ssig;
	
	program_name = argv[0];
	fimg = fpub = fsig = NULL;
	spub = ssig = NULL;
	exit_stat = 1;

	// Validate input parameters count
	if (argc - 1 != 3) {
		help();
		exit(1);
	}

	if (open_file(&fimg, argv[1])) {
		exit_stat = 2;
		goto prog_exit;
	}
	if (open_file(&fpub, argv[2])) {
		exit_stat = 2;
		goto prog_exit;
	}
	if (open_file(&fsig, argv[3])) {
		exit_stat = 2;
		goto prog_exit;
	}

	// Compute SHA-256 hash of firmware 
	// reference: https://stackoverflow.com/questions/63051416/hash-a-file-with-sha-on-a-memory-constrained-system-using-mbedlts
	mbedtls_sha256_context ctx;
	mbedtls_sha256_init(&ctx);
	mbedtls_sha256_starts_ret(&ctx, 0);

	uint8_t _sha_buf[BUFFER_SIZE];
	size_t read;
	while ((read = fread(_sha_buf, 1, BUFFER_SIZE, fimg)) > 0) {
		mbedtls_sha256_update_ret(&ctx, _sha_buf, read);
	}
	
	// compute final hash sum
	mbedtls_sha256_finish_ret(&ctx, hash);
	mbedtls_sha256_free(&ctx);
	
	// PUBLIC KEY
	// reference: https://forums.mbed.com/t/parsing-ec-keys-created-with-openssl/4527
	len = (get_file_size(fpub));
	spub =calloc(len + 1, 1);
	if (spub == NULL) {
		fprintf(stderr, "memory allocation failed!\n");
		exit_stat = 3;
		goto prog_exit;
	}
	fread(spub, 1, len, fpub);
	
	mbedtls_pk_context publicKey;
	mbedtls_pk_init(&publicKey);
	ret = mbedtls_pk_parse_public_key(&publicKey, (const unsigned char *)spub, len + 1);
	if (ret != 0) {
		fprintf(stderr, "parse public key failed err: %d\n", ret);
		exit_stat = 4;
		goto prog_exit;
	}

	// SIGNATURE
	len = (get_file_size(fsig));
	ssig = calloc(len + 1, 1);
	if (ssig == NULL) {
		fprintf(stderr, "memory allocation failed!\n");
		exit_stat = 5;
		goto prog_exit;
	}
	fread(ssig, 1, len, fsig);

	// VERIFY IMAGE against SHA256 & SIGNATURE
	ret = mbedtls_pk_verify(&publicKey, MBEDTLS_MD_SHA256, hash, 0, (const unsigned char *)ssig, len);
	if (ret != 0) {
		fprintf(stderr, "Invalid signature err: %d!\n", ret);
		exit_stat = 6;
		goto prog_exit;
	}
	printf ("Signature is valid\n");
	exit_stat = 0;
prog_exit:
	free(ssig);
	free(spub);
	if (fimg) fclose(fimg);
	if (fpub) fclose(fpub);
	if (fsig) fclose(fsig);
	mbedtls_pk_free(&publicKey);
	
	exit(exit_stat);
}
